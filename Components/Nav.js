import { FormControl, NativeSelect, Tab, Tabs } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Badge from "@material-ui/core/Badge";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { createStyles, fade, makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MailIcon from "@material-ui/icons/Mail";
import MoreIcon from "@material-ui/icons/MoreVert";
import NotificationsIcon from "@material-ui/icons/Notifications";
import SearchIcon from "@material-ui/icons/Search";
import axios from "axios";
import "date-fns";
import React from "react";
import { useOvermind } from "../src/Utils/OvermindHelper";

const useStyles = makeStyles((theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      display: "none",
      [theme.breakpoints.up("sm")]: {
        display: "block",
      },
    },
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(3),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "20ch",
      },
    },
    sectionDesktop: {
      display: "none",
      [theme.breakpoints.up("md")]: {
        display: "flex",
      },
    },
    sectionMobile: {
      display: "flex",
      [theme.breakpoints.up("md")]: {
        display: "none",
      },
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 219,
      position: "absolute",
      top: -7,
      left: 431,
      maxWidth: 200,
      "&:hover": {
        ".selectEmpty": {
          display: "block",
        },
      },
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);
const Nav = () => {
  const { state, actions } = useOvermind();
  const [searchText, setSearchText] = React.useState();

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    //
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {state.isLoggedIn && (
        <MenuItem
          onClick={() => {
            history.push("/profile");
          }}
        >
          Profile
        </MenuItem>
      )}
      {state.isLoggedIn && !state.pending && (
        <MenuItem
          onClick={() => {
            history.push("/register-as-doctor");
          }}
        >
          Register As A Doctor
        </MenuItem>
      )}
      {state.isLoggedIn && state.pending && <MenuItem>Pending</MenuItem>}
      {/* {!state.pending && (
        <MenuItem
          onClick={() => {
            history.push("/register-as-doctor");
          }}
        >
          Register As A Doctor
        </MenuItem>
      )} */}
      <MenuItem
        onClick={() => {
          history.push("/profile");
        }}
      >
        Profile
      </MenuItem>
      {state.isLoggedIn && (
        <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      )}
      {!state.isLoggedIn && (
        <MenuItem>{/* <Link to="/login">Login</Link> */}</MenuItem>
      )}
      {!state.isLoggedIn && (
        <MenuItem
          onClick={() => {
            history.push("/register");
          }}
        >
          Register
        </MenuItem>
      )}
    </Menu>
  );
  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={11} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );
  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            MediSence
          </Typography>

          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              onChange={() => {}}
              inputProps={{ "aria-label": "search" }}
            />
          </div>
          <div>
            <FormControl className={classes.formControl} style={{}} open={true}>
              <NativeSelect
                name="age"
                className={classes.selectEmpty}
                inputProps={{ "aria-label": "age" }}
                onChange={(e) => {
                  actions.setSelectedCategoryTitle(e.target.value);
                  axios
                    .get(
                      `http://localhost:4000/posts/category/?category=${e.target.value}`
                    )
                    .then((res) => {
                      console.log("categoryData:", res.data);
                      actions.setSelectedCategory(res.data);
                    });
                }}
              >
                <option value="primary-medication" selected>
                  Primary Medication
                </option>
                <option value="trending-medical-news">
                  Trending Mdeical News
                </option>
                <option value="trending-doctors">Trending Doctors</option>
                <option value="trending-clinics">Trending Clinics</option>
              </NativeSelect>
            </FormControl>
          </div>
          <div
            style={{
              marginLeft: 245,
              color: "#fff",
            }}
          >
            <Tabs
              indicatorColor="primary"
              textColor="white"
              // onChange={handleChange}
              aria-label="disabled tabs example"
            >
              <Tab label="Home" />
              <Tab label="Feed" />
              <Tab label="Tips" />
            </Tabs>
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton>
            <IconButton aria-label="show 17 new notifications" color="inherit">
              <Badge badgeContent={17} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            {!state.hasGoogleResponse ? (
              <IconButton
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
            ) : (
              <IconButton
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
              >
                {/* {state.hasFacebookResppnse && (
                    <img
                      src={state?.facebookResponse?.picture?.data.url}
                      alt={state.facebookResponse.name}
                      style={{
                        height: 40,
                        widht: 40,
                        borderRadius: "50%",
                      }}
                    ></img>
                  )} */}
                {/* {state.hasGoogleResponse && (
                    <img
                      src={state?.googleResponse?.imageUrl}
                      alt={state.googleResponse.name}
                      style={{
                        height: 40,
                        widht: 40,
                        borderRadius: "50%",
                      }}
                    ></img>
                  )} */}
                <img
                  src={state?.googleResponse?.imageUrl}
                  alt={state.googleResponse.name}
                  style={{
                    height: 40,
                    widht: 40,
                    borderRadius: "50%",
                  }}
                ></img>
              </IconButton>
            )}
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
};

export default Nav;
