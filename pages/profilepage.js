import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import FavoriteOutlinedIcon from "@material-ui/icons/FavoriteOutlined";
import ThumbUpAltOutlinedIcon from "@material-ui/icons/ThumbUpAltOutlined";
import PropTypes from "prop-types";
import React from "react";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  // root: {
  //   display: "flex",
  //   backgroundColor: "#dfecf2",
  //   height: "100vh",
  //   alignItems: "center",
  //   justifyContent: "center",
  // },
  // container: {
  //   maxWidth: 980,
  //   height: "70vh",
  //   backgroundColor: "#89cff0",
  //   display: "flex",
  //   fontFamily: "Helvetica",
  // },
  // profileSidebar: {
  //   flex: 0.2,
  //   padding: 10,
  // },
  // profileImage: {
  //   display: "flex",
  //   justifyContent: "center",
  //   alignItems: "center",
  //   flexDirection: "column",
  //   padding: "10px 0",
  // },
  // profileText: {
  //   display: "flex",
  //   justifyContent: "center",
  //   alignItems: "center",
  //   flexDirection: "column",
  //   padding: "20px 0",
  //   borderBottom: "1px solid #d3eced",
  // },
  // profileTabs: {
  //   display: "flex",
  //   justifyContent: "center",
  //   alignItems: "center",
  //   flexDirection: "column",
  //   marginTop: 10,
  //   padding: 20,
  // },
  // tab: {
  //   padding: "10px",
  //   width: 100,
  //   textAlign: "center",
  //   backgroundColor: "#c2eef0",
  //   margin: "5px 0",
  //   cursor: "pointer",
  // },
  // profileContent: {
  //   backgroundColor: "#c4f5e0",
  //   flex: 0.8,
  //   height: "100%",
  // },
  // navAbout: {
  //   display: "flex",
  // },
  // navText: {
  //   flex: 0.5,
  //   textAlign: "left",
  //   padding: "10px 10px",
  // },
  // navLinks: {
  //   flex: 0.5,
  //   display: "flex",
  //   alignItems: "center",
  //   justifyContent: "flex-end",
  //   padding: "0px 10px",
  // },
  // Underline: {
  //   backgroundColor: "#edccca",
  //   width: "100%",
  //   height: 1,
  // },
  // aboutInfo: {
  //   display: "flex",
  //   // alignItems: "center",
  //   justifyContent: "flex-start",
  //   flexDirection: "column",
  //   padding: "10px",
  //   fontFamily: "Helvetica",
  // },
}));

const profilepage = () => {
  const ProfileStyles = useStyles();
  const [value, setValue] = React.useState("about");

  const about = (
    <div className={ProfileStyles.profileContent}>
      <div className={ProfileStyles.navAbout}>
        <h5 className={ProfileStyles.navText}>About</h5>
        <div className={ProfileStyles.navLinks}>
          <ThumbUpAltOutlinedIcon />
          <FavoriteOutlinedIcon />
        </div>
      </div>
      <div className={ProfileStyles.Underline}></div>
      <div className={ProfileStyles.aboutInfo}>
        <img
          src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTR8fHByb2ZpbGV8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
          alt="name"
          style={{
            height: 200,
            width: 150,
          }}
        />
        <div className="aboutInfoText">
          <h5>About User</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetur lorem ipsum dolor sit amet
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
            Velit.lorem20 Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Iste ratione saepe quos facere fugiat? Reiciendis eveniet
            autem adipisci ratione incidunt.
          </p>
        </div>
      </div>
    </div>
  );
  const experience = (
    <div className={ProfileStyles.profileContent}>
      <div className={ProfileStyles.navAbout}>
        <h5 className={ProfileStyles.navText}>Experience</h5>
        <div className={ProfileStyles.navLinks}>
          <ThumbUpAltOutlinedIcon />
          <FavoriteOutlinedIcon />
        </div>
      </div>
      <div className={ProfileStyles.Underline}></div>
      <div className={ProfileStyles.aboutInfo}>
        <div className="aboutInfoText">
          <h5>User Experience</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetur lorem ipsum dolor sit amet
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
            Velit.lorem20 Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Iste ratione saepe quos facere fugiat? Reiciendis eveniet
            autem adipisci ratione incidunt.
          </p>
        </div>
      </div>
    </div>
  );
  const projects = (
    <div className={ProfileStyles.profileContent}>
      <div className={ProfileStyles.navAbout}>
        <h5 className={ProfileStyles.navText}>Projects</h5>
        <div className={ProfileStyles.navLinks}>
          <ThumbUpAltOutlinedIcon />
          <FavoriteOutlinedIcon />
        </div>
      </div>
      <div className={ProfileStyles.Underline}></div>
      <div className={ProfileStyles.aboutInfo}>
        <div className="aboutInfoText">
          <h5>User Projects</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetur lorem ipsum dolor sit amet
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
            Velit.lorem20 Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Iste ratione saepe quos facere fugiat? Reiciendis eveniet
            autem adipisci ratione incidunt.
          </p>
        </div>
      </div>
    </div>
  );
  const getRightSideContent = (value) => {
    switch (value) {
      case "about":
        return about;
      case "experience":
        return experience;
      case "projects":
        return projects;
      default:
        return "No Content";
    }
  };
  return (
    <div className={ProfileStyles.root}>
      <Paper>
        <div className={ProfileStyles.container}>
          <div className={ProfileStyles.profileSidebar}>
            <div className={ProfileStyles.profileImage}>
              <img
                src="https://image.freepik.com/free-vector/man-profile-cartoon_18591-58482.jpg"
                alt="profile"
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: "50%",
                }}
              />
              <div className={ProfileStyles.profileText}>
                <h5
                  style={{
                    margin: 0,
                    padding: 0,
                  }}
                >
                  User Name
                </h5>
                <p>Short Description</p>
              </div>
            </div>
            <div className={ProfileStyles.profileTabs}>
              <p className={ProfileStyles.tab}>
                <Button
                  onClick={() => {
                    setValue("about");
                  }}
                >
                  About
                </Button>
              </p>
              <p className={ProfileStyles.tab}>
                <Button
                  onClick={() => {
                    setValue("experience");
                  }}
                >
                  Experience
                </Button>
              </p>

              <p className={ProfileStyles.tab}>
                <Button
                  onClick={() => {
                    setValue("projects");
                  }}
                >
                  Projects
                </Button>
              </p>
            </div>
          </div>
          {getRightSideContent(value)}
        </div>
      </Paper>
    </div>
  );
};

export default profilepage;
