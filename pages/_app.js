import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";
import { NextSeo } from "next-seo";
import { Provider } from "overmind-react";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { overmind } from "../src/Utils/OvermindHelper";
import { theme } from "../src/Utils/Theme";

React.useLayoutEffect = React.useEffect;

const _app = (props) => {
  const { Component, pageProps } = props;

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <Provider value={overmind}>
      <React.Fragment>
        <NextSeo title={"Our App"} />
        <ThemeProvider theme={theme}>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <Component {...pageProps} />
        </ThemeProvider>
      </React.Fragment>
    </Provider>
  );
};

_app.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};

export default _app;
