import { Button, Container, Grid, Paper } from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import React, { useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import Nav from "../Components/Nav";
import { useOvermind } from "../src/Utils/OvermindHelper";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
      margin: "20px 0",
    },
    divStyle: {
      height: 250,
      width: 300,
      background: "#e3dada",

      alignItems: "center",
    },
    paper: {
      height: 250,
      width: 300,
    },
    gridList: {
      flexWrap: "nowrap",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
    title: {
      color: theme.palette.primary.light,
    },
    titleBar: {
      background:
        "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
    },
  })
);

export default function tips() {
  const history = useHistory();
  const { state, actions } = useOvermind();
  const classes = useStyles();
  useEffect(() => {
    getFitnesstips();
    getFoodtips();
  }, []);

  // getting primaryMedication data
   function getFitnesstips() {
    axios
      .get("http://localhost:4000/posts/category?category=fitness-tips")
      .then((response) => {
        console.debug(response.data);
        // actions.setFitnesstips(response.data);
      });
  }

  const getFoodtips = () => {
    axios
      .get("http://localhost:4000/posts/category?category=food-tips")
      .then((response) => {
        console.debug(response.data);
        // actions.setFoodtips(response.data);
      });
  };
  return (
    <div>
      <Nav />
      <Container>
        <Grid
          container
          spacing={3}
          style={{
            marginTop: 5,
          }}
        >
          <Grid item xs={10}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              Fitness tips.....
            </h5>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "right",
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
                history.push("/show-more/fitness-tips");
              }}
            >
              See More
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {/* {state.fitnesstips.map((fitnessTip) => {
            return (
              <Grid item xs={3} style={{ marginLeft: 10 }}>
                <Paper className={classes.paper} elevation={10}>
                  <div className={classes.divStyle}>
                    <img
                      src={fitnessTip.image}
                      style={{ height: "70%", width: "100%" }}
                      alt={fitnessTip.title}
                    ></img>
                    <div
                      style={{
                        position: "relative",
                        top: -23,
                        left: 6,
                      }}
                    >
                      <h6>{fitnessTip.title}</h6>
                      <p>{fitnessTip.description}</p>
                    </div>
                  </div>
                </Paper>
              </Grid>
            );
          })} */}
        </Grid>
        {/* Second Row */}
        <Grid
          container
          spacing={3}
          style={{
            marginTop: "10px",
          }}
        >
          <Grid item xs={10}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              {" "}
              Food tips.....
            </h5>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "right",
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
                history.push("show-more/food-tips");
              }}
            >
              See More
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {/* {state.foodtips.map((foodTip) => {
            return (
              <Link to={`details/${foodTip.slug}`}>
                <Grid item xs={3} style={{ marginLeft: 10 }}>
                  <Paper className={classes.paper} elevation={10}>
                    <div className={classes.divStyle}>
                      <img
                        src={foodTip.image}
                        style={{ height: "70%", width: "100%" }}
                        alt={foodTip.title}
                      ></img>
                      <div
                        style={{
                          position: "relative",
                          top: -23,
                          left: 6,
                        }}
                      >
                        <h6>{foodTip.title}</h6>
                        <p>{foodTip.description}</p>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              </Link>
            );
          })} */}
        </Grid>
      </Container>
    </div>
  );
}
