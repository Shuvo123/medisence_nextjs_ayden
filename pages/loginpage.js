import { Container, Grid, Paper } from "@material-ui/core";
import React from "react";
import FacebookLogin from "react-facebook-login";
import GoogleLogin from "react-google-login";
import { useHistory } from "react-router-dom";
import { useOvermind } from "../src/Utils/OvermindHelper";
import Nav from "../Components/Nav";

const loginpage = () => {
  const { state, actions } = useOvermind();
  const history = useHistory();
  const componentClicked = () => {
    console.log("clicked");
  };

  const handleFailure = (response) => {
    console.log(response);
  };

  const responseFacebook = (response) => {
    actions.responseFacebook(response);
    console.log(response);
    history.push("/register");
  };
  const responseGoogle = (response) => {
    console.log(response.profileObj);
    actions.responseGoogle(response.profileObj);
    history.push("/register");
  };
  return (
    <div>
      <Nav></Nav>
      <Container>
        <Grid container spacing={3} direction="column">
          <Grid
            item
            xs={12}
            style={{
              padding: 161,
            }}
          >
            <Paper
              style={{
                padding: 90,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  position: "relative",
                  left: 5,
                  top: -59,
                }}
              >
                <FacebookLogin
                  appId="3614969825287785"
                  autoLoad={false}
                  fields="name,email,picture"
                  onClick={componentClicked}
                  callback={responseFacebook}
                />
              </div>
              <div
                style={{
                  position: "relative",
                  left: -155,
                  top: 8,
                }}
              >
                <GoogleLogin
                  clientId="60299092422-sk02doll1q3vbb7pf10k45472kj7uhrb.apps.googleusercontent.com"
                  buttonText="Login"
                  onSuccess={responseGoogle}
                  onFailure={handleFailure}
                  cookiePolicy={"single_host_origin"}
                />
              </div>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default loginpage;
