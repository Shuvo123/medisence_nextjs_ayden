import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Container,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Radio,
  RadioGroup,
  Select,
  TextField,
} from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import { makeStyles } from "@material-ui/core/styles";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import "date-fns";
import React from "react";
import Nav from "../Components/Nav";
import { ServerCalling } from "../Logics/ServerCalling";
import { useOvermind } from "../src/Utils/OvermindHelper";

const useStyles = makeStyles((theme) => ({
  text: {
    padding: 9,
  },
}));
const Register = () => {
  const { actions, state } = useOvermind();
  const [selectedDate, setSelectedDate] = React.useState();
  const [regData, setRegData] = React.useState({
    phone: "",
    brith_date: "",
    gender: "",
    age: "",
    location: "",
    weight: "",
    height: "",
    type: "",
    role: "",
    password: "",
  });
  const handleDateChange = (date) => {
    setSelectedDate(date);
    setRegData({ ...regData, brith_date: date });
  };

  const formSubmitHandler = (e) => {
    e.preventDefault();
    console.log(regData);
    ServerCalling.SendRegisterData(regData, actions, state, history);
  };
  const classes = useStyles();
  return (
    <div>
      <Nav />
      <Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{
            marginTop: "10px",
          }}
        >
          <Grid item xs={6}>
            <Paper
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                padding: 20,
              }}
            >
              <div className="register_div">
                <form
                  onSubmit={(e) => {
                    formSubmitHandler(e);
                  }}
                >
                  <Grid
                    item
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Phone...."
                        className={classes.text}
                        onChange={(e) => {
                          setRegData({ ...regData, phone: e.target.value });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Age...."
                        className={classes.text}
                        onChange={(e) => {
                          setRegData({ ...regData, age: e.target.value });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Location...."
                        className={classes.text}
                        onChange={(e) => {
                          setRegData({ ...regData, location: e.target.value });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="weight...."
                        className={classes.text}
                        onChange={(e) => {
                          setRegData({ ...regData, weight: e.target.value });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="height...."
                        className={classes.text}
                        onChange={(e) => {
                          setRegData({ ...regData, height: e.target.value });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          margin="normal"
                          id="date-picker-dialog"
                          label="Birth Date"
                          format="MM/dd/yyyy"
                          value={selectedDate}
                          onChange={handleDateChange}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider> */}
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl
                        component="fieldset"
                        className={classes.text}
                      >
                        <FormLabel component="legend">Gender</FormLabel>
                        <RadioGroup
                          row
                          aria-label="gender"
                          name="gender1"
                          // value={value}
                          onChange={(e) => {
                            setRegData({ ...regData, gender: e.target.value });
                          }}
                        >
                          <FormControlLabel
                            value="female"
                            control={<Radio />}
                            label="Female"
                          />
                          <FormControlLabel
                            value="male"
                            control={<Radio />}
                            label="Male"
                          />
                        </RadioGroup>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl className={classes.text}>
                        <InputLabel id="demo-simple-select-helper-label">
                          Role
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-helper-label"
                          id="demo-simple-select-helper"
                          style={{
                            minWidth: "140px",
                          }}
                          // value={age}
                          onChange={(e) => {
                            setRegData({ ...regData, role: e.target.value });
                          }}
                        >
                          <MenuItem value="">
                            <em>None</em>
                          </MenuItem>
                          <MenuItem value="user">user</MenuItem>
                          <MenuItem value="admin">admin</MenuItem>
                          <MenuItem value="blocked">blocked</MenuItem>
                          <MenuItem value="doctor">doctor</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl
                        component="fieldset"
                        className={classes.text}
                      >
                        <FormLabel component="legend">Type</FormLabel>
                        <RadioGroup
                          row
                          aria-label="gender"
                          name="gender1"
                          // value={value}
                          onChange={(e) => {
                            setRegData({
                              ...regData,
                              type: e.target.value,
                            });
                          }}
                        >
                          <FormControlLabel
                            value="people"
                            control={<Radio />}
                            label="people"
                          />
                          <FormControlLabel
                            value="doctor"
                            control={<Radio />}
                            label="doctor"
                          />
                        </RadioGroup>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="password"
                        variant="outlined"
                        label="Password...."
                        className={classes.text}
                        onChange={(e) => {
                          setRegData({
                            ...regData,
                            password: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      className={classes.button}
                      endIcon={<Icon>send</Icon>}
                    >
                      Send
                    </Button>
                  </Grid>
                </form>
              </div>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Register;
