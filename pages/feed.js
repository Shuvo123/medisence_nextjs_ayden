import {
  AppBar,
  Avatar,
  Container,
  Grid,
  Paper,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { deepOrange } from "@material-ui/core/colors";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogActions from "@material-ui/core/DialogActions";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from "@material-ui/core/styles";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import React, { useEffect, useState } from "react";
// import "../../Css/feed.css";
import { useOvermind } from "../src/Utils/OvermindHelper";
import Nav from "../Components/Nav";

// AntTabs
const AntTabs = withStyles({
  root: {
    borderBottom: "1px solid #e8e8e8",
  },
  indicator: {
    backgroundColor: "#1890ff",
  },
})(Tabs);

// AntTab
// const AntTab = withStyles((theme) =>
//   createStyles({
//     root: {
//       textTransform: "none",
//       minWidth: 72,
//       fontWeight: theme.typography.fontWeightRegular,
//       marginRight: theme.spacing(4),
//       fontFamily: [
//         "-apple-system",
//         "BlinkMacSystemFont",
//         '"Segoe UI"',
//         "Roboto",
//         '"Helvetica Neue"',
//         "Arial",
//         "sans-serif",
//         '"Apple Color Emoji"',
//         '"Segoe UI Emoji"',
//         '"Segoe UI Symbol"',
//       ].join(","),
//       "&:hover": {
//         color: "#40a9ff",
//         opacity: 1,
//       },
//       "&$selected": {
//         color: "#1890ff",
//         fontWeight: theme.typography.fontWeightMedium,
//       },
//       "&:focus": {
//         color: "#40a9ff",
//       },
//     },
//     selected: {},
//   })
// )((props: StyledTabProps) => <Tab disableRipple {...props} />);

// interface StyledTabProps {
//   label: string;
// }

// const StyledTab = withStyles((theme: Theme) =>
//   createStyles({
//     root: {
//       textTransform: "none",
//       color: "#fff",
//       fontWeight: theme.typography.fontWeightRegular,
//       fontSize: theme.typography.pxToRem(15),
//       marginRight: theme.spacing(1),
//       "&:focus": {
//         opacity: 1,
//       },
//     },
//   })
// )((props: StyledTabProps) => <Tab disableRipple {...props} />);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  padding: {
    //   padding: theme.spacing(3),
  },
  demo1: {
    backgroundColor: theme.palette.background.paper,
  },
  demo2: {
    backgroundColor: "#2e1534",
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

// feed Compnent
const feed = () => {
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const [tabValue, setTabValue] = useState(0);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [users, setUsers] = useState([]);
  const [doctors, setDoctors] = useState([]);
  const { state, actions } = useOvermind();
  // getting users and doctors
  useEffect(() => {
    getUsers();
    getDoctors();
    getfeedData();
  }, []);
  const getUsers = () => {
    axios.get("http://localhost:4000/users/role?role=user").then((response) => {
      console.log(response.data);
      setUsers(response.data);
    });
  };
  const getDoctors = () => {
    axios
      .get("http://localhost:4000/users/role?role=doctor")
      .then((response) => {
        console.log(response.data);
        setDoctors(response.data);
      });
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleTitleChange = (e) => {
    setTitle(e.target.value);
  };
  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
  };
  const handleCategoryChange = (e) => {
    setCategory(e.target.value);
  };
  const handleImageChange = (e) => {
    setImage(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:4000/posts", {
        title,
        description,
        category,
        image,
      })
      .then((response) => {
        console.log(response);
        setTitle("");
        setDescription("");
        setCategory("");
        setImage("");
        setOpen(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const getfeedData = () => {
    axios.get("http://localhost:4000/posts").then((response) => {
      // console.log(response.data)
      // actions.setfeedData(response.data);
    });
  };
  const getTabChange = (tabIndex) => {
    switch (tabIndex) {
      case 0:
        return users.map((user) => {
          return (
            <Grid item xs={12}>
              <Paper
                style={{
                  padding: 10,
                  textAlign: "center",
                  // height: "10vh",
                  backgroundColor: "#d5e1f2",
                }}
              >
                <div className="info-container">
                  <Grid item container justify="space">
                    <Grid item xs={6}>
                      {" "}
                      <Avatar
                        style={{
                          marginTop: 14,
                          marginLeft: 10,
                          backgroundColor: "#ed7351",
                        }}
                      ></Avatar>
                    </Grid>
                    <Grid item xs={6}>
                      <h5 style={{ color: "#6c7275" }}>{user.name}</h5>
                    </Grid>
                  </Grid>
                </div>
              </Paper>
            </Grid>
          );
        });

      case 1:
        return doctors.map((doctor) => {
          return (
            <Grid item xs={12}>
              <Paper
                style={{
                  padding: 10,
                  textAlign: "center",
                  // height: "10vh",
                  backgroundColor: "#d5e1f2",
                }}
              >
                <div className="info-container">
                  <Grid item container justify="space">
                    <Grid item xs={6}>
                      {" "}
                      <Avatar
                        style={{
                          marginTop: 14,
                          marginLeft: 10,
                          backgroundColor: "",
                        }}
                      ></Avatar>
                    </Grid>
                    <Grid item xs={6}>
                      <h5 style={{ color: "#6c7275" }}>{doctor.name}</h5>
                    </Grid>
                  </Grid>
                </div>
              </Paper>
            </Grid>
          );
        });

      default:
        return "No value here";
    }
  };

  // const handleTabChange = (event,index) => {
  //   setTabValue(index);
  // }
  return (
    <div>
      <Nav/>
      <Container>
        <Grid
          container
          style={{
            marginTop: 10,
          }}
          spacing={3}
        >
          <Grid item xs={3}>
            <Paper style={{}}>
              <div id="headerDiv">
                {/* <AntTabs aria-label="ant example">
                  <AntTab label="Sponsord Doctors" />
                </AntTabs> */}
              </div>
              <div
                style={{
                  backgroundColor: "#d5e1f2",
                  height: "10vh",
                }}
                className="sponsoredDoctors"
              >
                <div className="sponsoredDoctors_img">
                  <Avatar />
                </div>
                <div className="sponsoredDoctors_info">
                  <h5>Name</h5>
                  <p>
                    MBBS,FCPS from<span>DMC</span>{" "}
                  </p>
                </div>
              </div>
              <div
                style={{
                  backgroundColor: "#d5e1f2",
                  height: "10vh",
                }}
                className="sponsoredDoctors"
              >
                <div className="sponsoredDoctors_img">
                  <Avatar />
                </div>
                <div className="sponsoredDoctors_info">
                  <h5>Name</h5>
                  <p>
                    MBBS,FCPS from<span>DMC</span>{" "}
                  </p>
                </div>
              </div>
              <div
                style={{
                  backgroundColor: "#d5e1f2",
                  height: "10vh",
                }}
                className="sponsoredDoctors"
              >
                <div className="sponsoredDoctors_img">
                  <Avatar />
                </div>
                <div className="sponsoredDoctors_info">
                  <h5>Name</h5>
                  <p>
                    MBBS,FCPS from<span>DMC</span>{" "}
                  </p>
                </div>
              </div>
              <div
                style={{
                  backgroundColor: "#d5e1f2",
                  height: "10vh",
                }}
                className="sponsoredDoctors"
              >
                <div className="sponsoredDoctors_img">
                  <Avatar />
                </div>
                <div className="sponsoredDoctors_info">
                  <h5>Name</h5>
                  <p>
                    MBBS,FCPS from<span>DMC</span>{" "}
                  </p>
                </div>
              </div>
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper>
              <div
                style={{
                  display: "flex",
                  padding: "10px 10px"
                }}
              >
                <div
                  className="postHeader"
                  style={{
                    flex: 0.9,
                    marginLeft: 10
                  }}
                >
                  <h5 style={{ 
                    marginLeft: 10,
                    fontsize: 20,
                    color: "gray",
                    fontWeight: 400,
                    marginTop: 0
                  }}>Posts</h5>
                </div>
                <div
                  id="headerDiv"
                  className="addPostBtn"
                  style={{
                    flex: 0.1,
                    alignItems: "right",
                  }}
                >
                  <Button
                    style={{
                      border: "none",
                      outline: "none",
                      cursor: "pointer",
                      flex: 0.2,
                    }}
                    onClick={handleClickOpen}
                  >
                    <AddCircleOutlineIcon
                      style={{
                        fontSize: 30,
                        color: "#606060"
                      }}
                    />
                  </Button>
                  <Grid
                    item
                    container
                    justify="flex-end"
                    alignItems="center"
                    direction="row"
                  >
                    <Dialog
                      onClose={handleClose}
                      aria-labelledby="customized-dialog-title"
                      open={open}
                    >
                      <DialogTitle
                        id="customized-dialog-title"
                        onClose={handleClose}
                      >
                        Add Post
                      </DialogTitle>
                      <form onSubmit={handleSubmit}>
                        <DialogContent dividers>
                          <Grid item container spacing={3}>
                            <Grid item xs={6}>
                              <label>
                                title:
                                <input
                                  required
                                  type="text"
                                  // value={this.state.value}
                                  onChange={handleTitleChange}
                                />
                              </label>
                            </Grid>
                            <Grid item xs={6}>
                              <label>
                                description:
                                <textarea
                                  required
                                  type="text"
                                  // value={this.state.value}
                                  onChange={handleDescriptionChange}
                                />
                              </label>
                            </Grid>
                            <Grid item xs={6}>
                              <label>
                                category:
                                <select
                                  required
                                  onChange={handleCategoryChange}
                                >
                                  <option value="primary-medication">
                                    primary-medication
                                  </option>
                                  <option value="trending-mdeical-news">
                                    trending-mdeical-news
                                  </option>
                                  <option value="trending-doctors">
                                    trending-doctors
                                  </option>
                                  <option value="clinic">clinic</option>
                                  <option value="food-tips">food-tips</option>
                                  <option value="fitness-tips">
                                    fitness-tips
                                  </option>
                                  <option value="yoga-tips">yoga-tips</option>
                                  <option value="gym-tips">gym-tips</option>
                                  <option value="medicine-tips">
                                    medicine-tips
                                  </option>
                                </select>
                              </label>
                            </Grid>
                            <Grid item xs={6}>
                              <label>
                                image:
                                <input
                                  required
                                  type="text"
                                  onChange={handleImageChange}
                                />
                              </label>
                            </Grid>
                          </Grid>
                        </DialogContent>
                        <DialogActions>
                          <Button autoFocus type="submit" color="primary">
                            Submit
                          </Button>
                        </DialogActions>
                      </form>
                    </Dialog>
                  </Grid>
                </div>
              </div>
              {/* {state.feedDatas.map((data) => {
                return (
                  <Grid
                    container
                    alignItems="center"
                    justify="center"
                    direction="column"
                  >
                    <Grid item xs={12}>
                      <img src={data.image} alt={data.title} />
                    </Grid>
                    <Grid item xs={12} style={{ paddingBottom: 10 }}>
                      <h5>{data.title}</h5>
                      <p>{data.description}</p>
                      <hr></hr>
                    </Grid>
                  </Grid>
                );
              })} */}
               <Grid
                    container
                    alignItems="center"
                    justify="center"
                    direction="column"
                  >
                    <Grid item xs={12}>
                      {/* <img src={data.image} alt={data.title} /> */}
                    </Grid>
                    <Grid item xs={12} style={{ paddingBottom: 10 }}>
                      {/* <h5>{data.title}</h5>
                      <p>{data.description}</p> */}
                      <hr></hr>
                    </Grid>
                  </Grid>
            </Paper>
          </Grid>
          <Grid item xs={3}>
            <Paper>
              <div></div>
            </Paper>

            <div className={classes.demo1}>
              {/* <AntTabs aria-label="ant example" value={tabValue} onChange={(newValue)=>{
                setTabValue(newValue);
              }}>
                <AntTab label="Users" />
                
                <AntTab label="Doctors" />
              </AntTabs> */}
              <AppBar position="static" color="default">
                <Tabs
                  value={tabValue}
                  onChange={(event, index) => {
                    setTabValue(index);
                  }}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab label="Users" />
                  <Tab label="Doctors" />
                </Tabs>
              </AppBar>
              {/* <Typography className={classes.padding} /> */}
              <Grid item container direction="column" spacing={1}>
                {getTabChange(tabValue)}
              </Grid>
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default feed;
