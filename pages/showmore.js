import { Button, Container, Grid, Paper } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import Pagination from "@material-ui/lab/Pagination";
import axios from "axios";
import React, { useEffect, useState } from "react";

import { useOvermind } from "../src/Utils/OvermindHelper";
import Nav from "../Components/Nav";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
      margin: "20px 0",
    },
    divStyle: {
      height: 250,
      width: 300,
      background: "#e3dada",

      alignItems: "center",
    },
    paper: {
      height: 250,
      width: 300,
    },
    gridList: {
      flexWrap: "nowrap",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
    title: {
      color: theme.palette.primary.light,
    },
    titleBar: {
      background:
        "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
    },
  })
);

const showmore = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [allPosts, setAllPosts] = useState([]);
  const { state, actions } = useOvermind();
  const classes = useStyles();
  
  // const { category } = useParams();
  useEffect(() => {
    getPrimaryMedication();
    getAllPostsByCategory();
  }, []);
  // getting primaryMedication data
  async function getPrimaryMedication() {
    axios
      // .get(
      //   `http://localhost:4000/posts/paginate?category=${category}&page=${
      //     currentPage - 1
      //   }`
      // )

      // git test
  }

  const getAllPostsByCategory = () => {
    axios
      // .get(`http://localhost:4000/posts/category/?category=${category}`)
      // .then((response) => {
      //   console.log(response.data);
      //   setAllPosts(response.data);
      // });
  };

  // Get Current Posts
  // const indexOfLastPost = currentPage * postsPerPage;
  // const indexOfFirstPost = indexOfLastPost - postsPerPage;
  // const currentPosts = state.posts.slice(indexOfFirstPost, indexOfLastPost);

  // // Paginate
  // const paginate = (number) => {
  //   setCurrentPage(number);
  // };
  const pageNumbers = [];
  const totalPosts = allPosts.length;
  for (let i = 1; i <= Math.ceil(totalPosts / 4); i++) {
    pageNumbers.push(i);
  }
  let count = 0;
  pageNumbers.map(() => {
    return count++;
  });
  return (
    <div>
      <Nav />
      <Container>
        <Grid
          container
          spacing={3}
          style={{
            marginTop: 5,
          }}
        >
          <Grid item xs={8}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              {/* {category.toUpperCase()}........... */}
            </h5>
          </Grid>
          <Grid
            xs={2}
            style={{
              position: "relative",
              top: 16,
              left: 176,
            }}
          >
            <Button
              variant="contained"
              size="small"
              color="primary"
              className={classes.margin}
              startIcon={<KeyboardBackspaceIcon />}
              onClick={() => {
                history.push("/");
              }}
            >
              Back
            </Button>
          </Grid>
          <Grid
            xs={2}
            style={{
              position: "relative",
              top: 16,
              left: 52,
            }}
          >
            <Button
              variant="contained"
              size="small"
              color="primary"
              endIcon={<ArrowRightAltIcon />}
              className={classes.margin}
            >
              Next
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {state.posts.map((post, index) => {
            return (
              <Grid item xs={3}>
                <Paper className={classes.paper} elevation={10}>
                  <div className={classes.divStyle}>
                    <img
                      src={post.image}
                      style={{ height: "70%", width: "100%" }}
                      alt={post.title}
                    ></img>
                    <div
                      style={{
                        position: "relative",
                        top: -23,
                        left: 6,
                      }}
                    >
                      <h6>{post.title}</h6>
                      <p>{post.description}</p>
                    </div>
                  </div>
                </Paper>
              </Grid>
            );
          })}
        </Grid>
        <Grid
          item
          xs={12}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: 20,
          }}
        >
          <Pagination
            count={count}
            onChange={(e, page) => {
              getPrimaryMedication();
              setCurrentPage(page);
            }}
            color="primary"
          />
        </Grid>
        {/* {category} */}
      </Container>
    </div>
  );
};

export default showmore;
