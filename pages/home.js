import { Button, Container, Grid, Paper } from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import React, { useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import Nav from "../Components/Nav";
import { useOvermind } from "../src/Utils/OvermindHelper";
// import Sliderhome from "./Sliderhome";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
      margin: "20px 0",
    },
    divStyle: {
      height: 250,
      width: 300,
      background: "#e3dada",

      alignItems: "center",
    },
    paper: {
      height: 250,
      width: 300,
    },
    gridList: {
      flexWrap: "nowrap",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
    title: {
      color: theme.palette.primary.light,
    },
    titleBar: {
      background:
        "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
    },
  })
);

export default function home() {
  const history = useHistory();
  const classes = useStyles();
  const { state, actions } = useOvermind();

  useEffect(() => {
    getPrimaryMedication();
    gettrendingMedicalNewses();
    getTrendingDoctors();
    getClinics();
    // console.log("hello");
  }, []);

  // getting primaryMedication data
  async function getPrimaryMedication() {
    axios
      .get("http://localhost:4000/posts/category?category=primary-medication")
      .then((response) => {
        // console.log(response.data);
        actions.setPrimaryMedication(response.data);
      });
  }

  const gettrendingMedicalNewses = () => {
    axios
      .get(
        "http://localhost:4000/posts/category?category=trending-medical-news"
      )
      .then((response) => {
        console.log(response.data);
        actions.setTrendingMedicalNewses(response.data);
      });
  };

  const getTrendingDoctors = () => {
    axios
      .get("http://localhost:4000/posts/category?category=doctors")
      .then((response) => {
        console.log(response.data);
        actions.setTrendingDoctors(response.data);
      });
  };
  const getClinics = () => {
    axios
      .get("http://localhost:4000/posts/category?category=clinic")
      .then((response) => {
        console.log(response.data);
        actions.setClinics(response.data);
      });
  };

  return (
    <div>
      <Nav />
      {/* <Sliderhome /> */}
      <Container>
        {/* Categorize View */}
        {state.hasSelectedCategory && (
          <div>
            <Grid
              container
              spacing={3}
              style={{
                marginTop: 5,
              }}
            >
              <Grid item xs={10}>
                <h5
                  style={{
                    borderBottom: "1px solid cyan",
                  }}
                >
                  {state.selectedCategoryTitle.toUpperCase()}
                </h5>
              </Grid>
              <Grid
                item
                xs={2}
                style={{
                  textAlign: "right",
                }}
              >
                <Button
                  variant="contained"
                  onClick={() => {
                    history.push(`/show-more/${state.selectedCategoryTitle}`);
                  }}
                >
                  See More
                </Button>
              </Grid>
            </Grid>
            <Grid container spacing={3}>
              {state.selectedCategory.map((item, index) => {
                return (
                  <Link to={`/details/${item.slug}`}>
                    <Grid
                      item
                      xs={3}
                      style={{
                        marginLeft: 10,
                      }}
                    >
                      <Paper className={classes.paper} elevation={10}>
                        <div className={classes.divStyle}>
                          <img
                            src={item.image}
                            style={{ height: "70%", width: "100%" }}
                            alt={item.title}
                          ></img>
                          <div
                            style={{
                              position: "relative",
                              top: -23,
                              left: 6,
                            }}
                          >
                            <h6>{item.title}</h6>
                            <p>{item.description}</p>
                          </div>
                        </div>
                      </Paper>
                    </Grid>
                    {/* <PostDetails post={post}/> */}
                  </Link>
                );
              })}
            </Grid>
          </div>
        )}
        {/* Default View */}
        <Grid
          container
          spacing={3}
          style={{
            marginTop: 5,
          }}
        >
          <Grid item xs={10}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              Primary Medication.....
            </h5>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "right",
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
                // history.push("/show-more/primary-medication");
              }}
            >
              See More
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {state.posts.map((post, index) => {
            return (
              <Grid
                item
                xs={3}
                style={{
                  marginLeft: 10,
                }}
              >
                <Paper className={classes.paper} elevation={10}>
                  <div className={classes.divStyle}>
                    <img
                      src={post.image}
                      style={{ height: "70%", width: "100%" }}
                      alt={post.title}
                    ></img>
                    <div
                      style={{
                        position: "relative",
                        top: -23,
                        left: 6,
                      }}
                    >
                      <h6>{post.title}</h6>
                      <p>{post.description}</p>
                    </div>
                  </div>
                </Paper>
              </Grid>
            );
          })}
          <Grid
            item
            xs={3}
            style={{
              marginLeft: 10,
            }}
          >
            <Paper className={classes.paper} elevation={10}>
              {/* <div className={classes.divStyle}>
                <img
                  src={post.image}
                  style={{ height: "70%", width: "100%" }}
                  alt={post.title}
                ></img>
                <div
                  style={{
                    position: "relative",
                    top: -23,
                    left: 6,
                  }}
                >
                  <h6>{post.title}</h6>
                  <p>{post.description}</p>
                </div>
              </div> */}
            </Paper>
          </Grid>
        </Grid>
        {/* Second Row */}
        <Grid
          container
          spacing={3}
          style={{
            marginTop: "10px",
          }}
        >
          <Grid item xs={10}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              {" "}
              Treanding Medical News.....
            </h5>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "right",
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
               
              }}
            >
              See More
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {state.trendingMedicalNewses.map((news, index) => {
            return (
              
                <Grid item xs={3} style={{ marginLeft: 10 }}>
                  <Paper className={classes.paper} elevation={10}>
                    <div className={classes.divStyle}>
                      <img
                        src={news.image}
                        style={{ height: "70%", width: "100%" }}
                        alt={news.title}
                      ></img>
                      <div
                        style={{
                          position: "relative",
                          top: -23,
                          left: 6,
                        }}
                      >
                        <h6>{news.title}</h6>
                        {/* <h6>{news.type}</h6> */}
                        <p>{news.description}</p>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              
            );
          })}
        </Grid>

        {/* 4th row */}
        <Grid
          container
          spacing={3}
          style={{
            marginTop: "10px",
          }}
        >
          <Grid item xs={10}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              {" "}
              Treanding Doctors.....
            </h5>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "right",
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
               
              }}
            >
              See More
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {state.trendingDoctors.map((trendingDoctor, index) => {
            return (
               
                <Grid item xs={3} style={{ marginLeft: 10 }}>
                  <Paper className={classes.paper} elevation={10}>
                    <div className={classes.divStyle}>
                      <img
                        src={trendingDoctor.image}
                        style={{ height: "70%", width: "100%" }}
                        alt={trendingDoctor.title}
                      ></img>
                      <div
                        style={{
                          position: "relative",
                          top: -23,
                          left: 6,
                        }}
                      >
                        <h6>{trendingDoctor.title}</h6>
                        {/* <h6>{news.type}</h6> */}
                        <p>{trendingDoctor.description}</p>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              
            );
          })}
        </Grid>

        {/* 5th row */}
        <Grid
          container
          spacing={3}
          style={{
            marginTop: "10px",
          }}
        >
          <Grid item xs={10}>
            <h5
              style={{
                borderBottom: "1px solid cyan",
              }}
            >
              {" "}
              Clinics Near you....
            </h5>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "right",
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
                history.push("/show-more/clinic");
              }}
            >
              See More
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          {state.clinics.map((clinic, index) => {
            return (
              <Link to={`/details/${clinic._id}`}>
                <Grid item xs={3} style={{ marginLeft: 10 }}>
                  <Paper className={classes.paper} elevation={10}>
                    <div className={classes.divStyle}>
                      <img
                        src={clinic.image}
                        style={{ height: "70%", width: "100%" }}
                        alt={clinic.title}
                      ></img>
                      <div
                        style={{
                          position: "relative",
                          top: -23,
                          left: 6,
                        }}
                      >
                        <h6>{clinic.title}</h6>
                        {/* <h6>{news.type}</h6> */}
                        <p>{clinic.description}</p>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              </Link>
            );
          })}
        </Grid>
      </Container>
    </div>
  );
}
