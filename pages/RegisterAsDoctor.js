
import {
  Button,
  Container,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Paper,
  Radio,
  RadioGroup,
  TextField,
} from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import { makeStyles } from "@material-ui/core/styles";
// import {
//   KeyboardDatePicker,
//   MuiPickersUtilsProvider,
// } from "@material-ui/pickers";
import "date-fns";
import React from "react";

import { ServerCalling } from "../Logics/ServerCalling";
import { useOvermind } from "../src/Utils/OvermindHelper";
import Nav from "../Components/Nav";
const useStyles = makeStyles((theme) => ({
  text: {
    padding: 9,
  },
}));

const RegisterAsDoctor = () => {
  const classes = useStyles();

  const { actions } = useOvermind();
  const [selectedDate, setSelectedDate] = React.useState();
  const [doctorData, setDoctorgData] = React.useState({
    name: "",
    email: "",
    image: "",
    phone: "",
    brith_date: "",
    gender: "",
    age: "",
    institution: "",
    passing_year: "",
  });
  const handleDateChange = (date) => {
    setSelectedDate(date);
    setDoctorgData({ ...doctorData, brith_date: date });
  };

  const formSubmitHandler = (e) => {
    e.preventDefault();
    console.log(doctorData);
    ServerCalling.SendDoctorRegisterData(doctorData, actions, history);
  };
  return (
    <div>
      <Nav />
      <Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{
            marginTop: "10px",
          }}
        >
          <Grid item xs={6}>
            <Paper
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                padding: 20,
              }}
            >
              <div className="register_div">
                <form
                  onSubmit={(e) => {
                    formSubmitHandler(e);
                  }}
                >
                  <Grid
                    item
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Name...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            name: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="email"
                        variant="outlined"
                        label="Email...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            email: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Phone...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            phone: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Image...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            image: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Age...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            age: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Institution...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            institution: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        type="text"
                        variant="outlined"
                        label="Passing Year...."
                        className={classes.text}
                        onChange={(e) => {
                          setDoctorgData({
                            ...doctorData,
                            passing_year: e.target.value,
                          });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                    <TextField
                        type="text"
                        variant="outlined"
                        label="BirthDate"
                        className={classes.text}
                        onChange={(e) => {
                          // setDoctorgData({
                          //   ...doctorData,
                          //   passing_year: e.target.value,
                          // });
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl
                        component="fieldset"
                        className={classes.text}
                      >
                        <FormLabel component="legend">Gender</FormLabel>
                        <RadioGroup
                          row
                          aria-label="gender"
                          name="gender1"
                          // value={value}
                          onChange={(e) => {
                            setDoctorgData({
                              ...doctorData,
                              gender: e.target.value,
                            });
                          }}
                        >
                          <FormControlLabel
                            value="female"
                            control={<Radio />}
                            label="Female"
                          />
                          <FormControlLabel
                            value="male"
                            control={<Radio />}
                            label="Male"
                          />
                        </RadioGroup>
                      </FormControl>
                    </Grid>

                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      className={classes.button}
                      endIcon={<Icon>send</Icon>}
                    >
                      Send
                    </Button>
                  </Grid>
                </form>
              </div>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default RegisterAsDoctor;
