import React, { useState, useEffect } from "react";
import { Grid, Paper, Container, Button } from "@material-ui/core";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import axios from "axios";
// import { useParams } from "react-router-dom";
const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
      margin: "20px 0",
    },
    divStyle: {
      height: 250,
      width: 300,
      background: "#e3dada",

      alignItems: "center",
    },
    paper: {
      height: 250,
      width: 300,
    },
    gridList: {
      flexWrap: "nowrap",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
    title: {
      color: theme.palette.primary.light,
    },
    titleBar: {
      background:
        "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
    },
  })
);

const postdetails = ({ post }) => {
  // const { slug } = useParams();
  const [data,setData] = useState({});
  const classes = useStyles();
  useEffect(()=>{
    getSiglePost();
  },[]);
  const getSiglePost = () => {
    // axios.get(`http://localhost:4000/posts/slug?slug=${slug}`)
    // .then((response)=>{
    //   console.log(response.data[0].description);
    //   setData(response.data[0]);
    // })
  }
  return (
    <div>
      <Grid item xs={3}>
        <Paper className={classes.paper} elevation={10}>
          <div className={classes.divStyle}>
            <img
              src={data.image}
              style={{ height: "70%", width: "100%" }}
              alt={data.title}
            ></img>
            <div
              style={{
                position: "relative",
                top: -23,
                left: 6,
              }}
            >
              <h6>{data.title}</h6>
              <p>{data.description}</p>
            </div>
          </div>
        </Paper>
      </Grid>
      {/* <p>{id}</p> */}
    </div>
  );
};

export default postdetails;
