import axios from "axios";
// const api_endpoint = "http://localhost:4000";
const api_endpoint = "http://localhost:4000";

//
const posts_endpoint = api_endpoint + "/posts";
const users_endpoint = `${api_endpoint}/users`;
const doctors_endpoint = `${api_endpoint}/doctors`;
export class ServerCalling {
  static getPosts(actions) {
    axios.get(posts_endpoint).then((response) => {
      actions.setFeedData(response.data);
    });
  }

  static SendRegisterData(data, actions, state, history) {
    state.hasGoogleResponse &&
      axios
        .post(users_endpoint, {
          name: state.googleResponse.name,
          email: state.googleResponse.email,
          image: state.googleResponse.imageUrl,
          phone: data.phone,
          brith_date: data.brith_date,
          gender: data.gender,
          age: data.age,
          location: data.location,
          weight: data.weight,
          height: data.height,
          type: data.type,
          role: data.role,
          password: data.password,
        })
        .then((res) => {
          console.log(res);
          if (res.status === 200) {
            // actions.setIsLoggedIn();
            history.push("/");
          }
        });
    state.hasFacebookResponse &&
      axios
        .post(users_endpoint, {
          name: state.facebookResponse.name,
          email: state.facebookResponse.email,
          image: state.facebookResponse.picture.data.url,
          phone: data.phone,
          brith_date: data.brith_date,
          gender: data.gender,
          age: data.age,
          location: data.location,
          weight: data.weight,
          height: data.height,
          type: data.type,
          role: data.role,
          password: data.password,
        })
        .then((res) => {
          if (res.status === 200) {
            // actions.setIsLoggedIn();
            history.push("/");
          }
        });
  }
  static SendDoctorRegisterData(data, actions, history) {
    axios
      .post(doctors_endpoint, {
        name: data.name,
        email: data.email,
        image: data.image,
        phone: data.phone,
        brith_date: data.brith_date,
        gender: data.gender,
        age: data.age,
        institution: data.institution,
        passing_year: data.passing_year,
      })
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          actions.setPending();
          history.push("/");
        }
      });
  }
}
