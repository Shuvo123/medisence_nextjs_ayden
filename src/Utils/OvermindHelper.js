// npm install overmind overmind-react
// yarn add overmind overmind-react
// import React from "react";
import { createOvermind } from "overmind";
import { createHook } from "overmind-react";

export const useOvermind = createHook();
export const overmind = createOvermind({
  state: {
    counter: 0,
    categories: [],
    googleResponse: {},
    facebookResponse: {},
    posts: [],
    hasFacebookResponse: false,
    hasGoogleResponse: false,
    trendingMedicalNewses: [],
    fitnessTips: [],
    foodTips: [],
    trendingDoctors: [],
    clinics: [],
    feedDatas: [],
    selectedCategory: [],
    hasSelectedCategory: false,
    selectedCategoryTitle: "",
    isLoggedInByGoogle: false,
    isLoggedInByFacebook: false,
    pending: false,
  },
  actions: {
    increase({ state }, number) {
      state.counter += number;
    },
    setCategories({ state }, data) {
      state.categories = data;
    },
    responseFacebook({ state }, response) {
      console.log(response);
      state.facebookResponse = response;
      state.hasFacebookResponse = true;
      state.hasGoogleResponse = false;
    },

    responseGoogle({ state }, response) {
      console.log(response);
      state.googleResponse = response;
      state.hasFacebookResponse = false;
      state.hasGoogleResponse = true;
    },

    setShowMoreCategory({ state }, data) {
      state.posts = data;
    },
    setPrimaryMedication({ state }, data) {
      state.posts = data;
    },

    setTrendingMedicalNewses({ state }, data) {
      state.trendingMedicalNewses = data;
    },
    setFitnessTips({ state }, data) {
      state.fitnessTips = data;
    },
    setFoodTips({ state }, data) {
      state.foodTips = data;
    },
    setTrendingDoctors({ state }, data) {
      state.trendingDoctors = data;
    },
    setClinics({ state }, data) {
      state.clinics = data;
    },
    setFeedData({ state }, data) {
      state.feedDatas = data;
    },
    setSelectedCategory({ state }, data) {
      state.selectedCategory = data;

      state.hasSelectedCategory = true;
    },
    setSelectedCategoryTitle({ state }, title) {
      state.selectedCategoryTitle = title;
    },
    setIsLoggedIn({ state }) {
      state.isLoggedInByGoogle = true;
      state.isLoggedInByFacebook = false;
    },
    setPending({ state }) {
      state.pending = true;
    },
  },
});
